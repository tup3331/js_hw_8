////////////////////////////////////////////////////////////////////////////////////////////



function isPalindrome(word) {
    let half = Math.floor(word.length / 2);

    for (let i = 0; i < half; i++) {
        if (word[i] == word[word.length - i - 1]) {
            return true;
        } else {
            return false;
        }
    }
}

isPalindrome(prompt("Введіть слово або рядок."));



////////////////////////////////////////////////////////////////////////////////////////////



function check(str, length) {
    let strLength = str.length;
    if (strLength <= length) {
        return true;
    } else {
        return false;
    }
}

check(prompt("Введіть рядок не більше 10 символів."), 10)



////////////////////////////////////////////////////////////////////////////////////////////



function age(bd) {
    const bDay = new Date(bd);
    const actDate = new Date();
    let subtract = actDate - bDay;
    let ageInDate = new Date(Number(subtract));
    let result = ageInDate.getUTCFullYear() - 1970;

    alert(`Повних років: ${result}`)
    return result;
}

age(prompt("Введіть дату народження у вигляді: <YYYY-MM-DD>"))